#!/usr/bin/env bash

OS=$(uname -s)
URL=https://raw.githubusercontent.com

echo ""
echo "Clean house"

drs="bin .config .vim"
fls=".bash_profile .bashrc .aliases .vimrc .base16_theme"

for i in $drs; do rm -rf $HOME/$i; done
for i in $fls; do rm -f $HOME/$i; done

echo ""
echo "Set symbolic links"
ln -sfn ~/.dotfiles/bin ~/bin
ln -sfn ~/.dotfiles/config ~/.config
ln -sfn ~/.dotfiles/vim ~/.vim
ln -sf ~/.dotfiles/vim/vimrc ~/.vimrc
ln -sf ~/.dotfiles/shell/bash_profile ~/.bash_profile
ln -sf ~/.dotfiles/shell/bashrc ~/.bashrc
ln -sf ~/.dotfiles/shell/aliases ~/.aliases

echo ""
echo "Source profile"
source ~/.bash_profile

if [ ! -f $BREW_PREFIX/bin/brew ]; then
  echo ""
  echo "Install Brew"
  [ $OS == "Darwin" ] && \
    /usr/bin/ruby -e "$(curl -fsSL $URL/Homebrew/install/master/install)"


  if [ $OS == "Linux" ]; then
    sudo passwd $USER
    sudo apt update -y && sudo apt upgrade -y
    sudo apt install linuxbrew-wrapper -y
    sudo apt install zlib1g-dev libssl-dev
    brew update && brew upgrade
    brew install bzip2 libffi libxml2 libxmlsec1 openssl readline sqlite xz zlib
  fi

fi
  
if [ ! -f $BREW_PREFIX/bin/bash ]; then
  echo ""
  echo "Install bash"
  brew install bash

  echo ""
  echo "Change shell to brew's bash. . ."
  echo "$BREW_PREFIX/bin/bash" | sudo tee -a /etc/shells > /dev/null
fi

if [ $SHELL != $BREW_PREFIX/bin/bash ]; then
  echo "$BREW_PREFIX/bin/bash" | sudo tee -a /etc/shells > /dev/null
  chsh -s "$BREW_PREFIX/bin/bash"
fi

echo ""
echo "Get a prompt and a few other goodies"
tools="starship exa bat most fzf rg ranger fd go cmake tmux"
/usr/local/opt/coreutils/libexec/gnubin
for i in $tools; do 
  echo $i
  if [ ! -f $BREW_PREFIX/bin/$i ]; then 
    brew install $i
    [ $i == "fzf" ] && $BREW_PREFIX/opt/fzf/install
  fi
done

echo ""
echo "Install GNU utilities"
tools="coreutils findutils gnu-tar gnu-sed gawk gnutls gnu-indent gnu-getopt grep"
for i in $tools; do 
  echo $i
  if [ ! -f $BREW_PREFIX/bin/$i ]; then 
    brew install $i
    [ $i == "fzf" ] && $BREW_PREFIX/opt/fzf/install
  fi
done

if [ ! -f $BREW_PREFIX/bin/nvim ]; then
  echo ""
  echo "Get an editor"
  brew install neovim
fi

if [ ! -d ~/.pyenv ]; then
  echo ""
  echo "Installing pyenv" 
  brew install pyenv 
  pyenv install 3.7.5
  pyenv install miniconda3-4.3.30
fi

if [ ! -d ~/.rbenv ]; then
  echo ""
  echo "Installing pyenv" 
  brew install rbenv 
  rbenv install --skip-existing 2.6.0
  rbenv rehash
fi
